/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.camel.component.cxf;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.cxf.jaxws.context.WrappedMessageContext;

public class CxfRawMessageRouterTest extends CxfSimpleRouterTest {
    private String routerEndpointURI = "cxf://" + ROUTER_ADDRESS + "?" + SERVICE_CLASS + "&dataFormat=MESSAGE";
    private String serviceEndpointURI = "cxf://" + SERVICE_ADDRESS + "?" + SERVICE_CLASS + "&dataFormat=MESSAGE";
    protected RouteBuilder createRouteBuilder() {
        return new RouteBuilder() {
            public void configure() {
                from(routerEndpointURI).to("log:org.apache.camel?level=DEBUG").to(serviceEndpointURI).to("mock:result");
            }
        };
    }
    
    public void testTheContentType() throws Exception {
        MockEndpoint result = getMockEndpoint("mock:result");
        result.reset();
        HelloService client = getCXFClient();
        client.echo("hello world");        
        Map context = (Map)result.assertExchangeReceived(0).getIn().getHeaders().get("ResponseContext"); 
        assertNotNull("Expect to get the protocal header ", context.get("org.apache.cxf.message.Message.PROTOCOL_HEADERS"));
        Map protocalHeaders = (Map) context.get("org.apache.cxf.message.Message.PROTOCOL_HEADERS");
        assertTrue("Should get the content type", protocalHeaders.get("content-type").toString().startsWith("[text/xml; charset="));
        assertEquals("Should get the response code ", context.get("org.apache.cxf.message.Message.RESPONSE_CODE"), 200);
    }
    
    public void testTheContentTypeOnTheWire() throws Exception {
        Exchange exchange = template.send(ROUTER_ADDRESS,  new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().setBody("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" 
                                         + "<soap:Body><ns1:echo xmlns:ns1=\"http://cxf.component.camel.apache.org/\">"
                                         + "<arg0 xmlns=\"http://cxf.component.camel.apache.org/\">hello world</arg0>"
                                         + "</ns1:echo></soap:Body></soap:Envelope>");
            }

        });        
        assertNotNull("We should get the content type here", exchange.getOut().getHeader("content-type"));
        assertTrue("Get wrong content type", ((String)exchange.getOut().getHeader("content-type")).startsWith("text/xml; charset="));
        String response = exchange.getOut().getBody(String.class);        
        assertNotNull("Response should not be null", response);
        assertTrue("We should get right return result", response.indexOf("echo hello world") > 0);
    }
}
